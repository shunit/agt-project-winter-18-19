import numpy as np


class Auctioneer(object):

    def __init__(self, number_of_bidders, d_support, d_probs, epsilon, T, bidder_chooser=np.argmin, track_rounds=True):
        self.number_of_bidders = number_of_bidders
        self.rounds = [0] * number_of_bidders           # counts the round number of each bidder
        self.d_support = d_support
        self.d_probs = d_probs
        self.rho = min(max(d_support), 1-epsilon/2)
        self.delta = (1-self.rho)/(1-min(d_support))
        self.T = int(T/number_of_bidders)
        # maybe bad idea to divide by number of bidders? Does nothing so far
        self.bidder_chooser = bidder_chooser
        self.track_rounds = track_rounds

        self.revenues = []
        self.allocations = []
        self.chosen_bidders = []
        self.num_real_arms = int(np.log2(epsilon/2)/np.log2(1-self.delta))
        num_arms = range(1, self.num_real_arms + 1)

        self.session_empty_rounds = [T] + [(1 - (1 - self.delta) ** (i - 1)) * T for i in num_arms]
        self.session_0_rounds = [0] + [((1 - self.rho) * (1 - self.delta) ** (i - 1)) * T for i in num_arms]
        self.session_1_rounds = [0] + [((1 - self.delta) ** (i - 1)) * self.rho * T for i in num_arms]

    def get_arms(self):
        return list(range(self.num_real_arms + 1))

    def auction_round(self, arms_indices):
        """Receives indices of arms pulled by each bidder, decides upon allocation and pricing for each bidder"""
        chosen_bidder = self.bidder_chooser(arms_indices)
        bid_tracker = chosen_bidder if self.track_rounds is True else 0
        chosen_arm = arms_indices[chosen_bidder]
        # Doesn't work. Maybe go for smallest bidder? Smaller arm -> more likely in 1 session.
        # Turns out choosing smaller bidder doesn't work either. Choosing randomly also fails.
        allocations_and_prices = [(0, 0)]*len(arms_indices)
        # print("Bidder number: {}, arm number: {}, round number: {}".format(chosen_bidder, chosen_arm, self.rounds[chosen_bidder]))
        # all 0 arms
        if arms_indices[chosen_bidder] == 0:
            allocation = 0
            price = 0
        # emptyset session
        elif self.rounds[bid_tracker] <= self.session_empty_rounds[chosen_arm]:
            allocation = 0
            price = 0
        # 0 session
        elif self.rounds[bid_tracker] <= self.session_empty_rounds[chosen_arm] + self.session_0_rounds[chosen_arm]:
            allocation = 1
            price = 0
        # 1 session
        else:
            allocation = 1
            price = 1

        allocations_and_prices[chosen_bidder] = (allocation, price)
        self.revenues.append(price)
        self.allocations.append(allocation)
        self.chosen_bidders.append(chosen_bidder)
        self.rounds[bid_tracker] += 1
        return allocations_and_prices

        # interesting: if we don't keep track of each bidder's rounds, profits actually increase.
        # i.e., don't keep a list of rounds, only a single number as in the 1-bidder case
    def get_revenues(self):
        return self.revenues

