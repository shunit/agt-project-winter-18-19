import numpy as np


class Bidder(object):

    def __init__(self, arms, d_support, d_probs, step_size):
        self.arms = arms
        self.d_support = d_support
        self.d_probs = d_probs
        self.vals = []
        self.weights = {}
        for v in d_support:
            self.weights[v] = {}
            for arm in arms:
                self.weights[v][arm] = 1
        self.step_size = step_size
        self.bids = []
        self.chosen_arms = []
        self.utilities = []
        self.allocations = []

    def draw_valuation(self):
        self.vals.append(np.random.choice(self.d_support, p=self.d_probs))

    def bid(self):
        weights = self.weights[self.vals[-1]]
        s = sum(weights.values())
        normalized = [w/s for w in weights.values()]
        chosen_arm = np.random.choice(list(weights.keys()), p=normalized)
        self.chosen_arms.append(chosen_arm)
        return chosen_arm

    def learn(self, allocation, price):
        """post bid activity, returns list of arm weights"""
        prev_weight = self.weights[self.vals[-1]][self.chosen_arms[-1]]
        utility = self.utility(allocation, price)
        # print("Price: {}, allocation: {}, valuation: {}, utility: {}".format(price, allocation, self.vals[-1], utility))
        self.utilities.append(utility)
        self.weights[self.vals[-1]][self.chosen_arms[-1]] = prev_weight*(1-self.step_size)**(-utility)
        return list(self.weights[self.d_support[0]].values())

    def utility(self, allocation, price):
        return self.vals[-1]*allocation - price
