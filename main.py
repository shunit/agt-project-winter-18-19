from bidder import Bidder
from auctioneer import Auctioneer
import numpy as np
import matplotlib.pyplot as plt


def experiment(number_of_bidders, d_support, d_probs, epsilon, T, step_size,
               bidder_chooser, track_rounds, show_graphs=True):
    print("Experiment parameters: number of bidders={}, epsilon={}, step_size={}, T={}, "
          "bidder chooser={}, track rounds={}".format(number_of_bidders, epsilon, step_size, T, bidder_chooser, track_rounds))
    colors = ['b', 'g', 'r', 'c', 'y', 'm', 'k', '#8E44AD', ]
    markers = ["x", ",", ".", "|", "3", "4", "*", ]

    # initialization
    a = Auctioneer(number_of_bidders, d_support, d_probs, epsilon, T, bidder_chooser, track_rounds)
    bs = []
    for _ in range(number_of_bidders):
        bs.append(Bidder(a.get_arms(), d_support, d_probs, step_size))

    revenues, weights = run_auction(a, bs)
    exp_val = np.dot(d_support, d_probs)
    arms = range(len(weights[0][0]))

    if show_graphs:
        # graph for weights
        weight_graph(colors, markers, arms, weights)

        # graph for sessions
        session_graph(a, arms)

    print("Results: Number of arms: {}, number of allocations={}, percentage of revenue={}".format(len(arms), sum(a.allocations), sum(revenues)/(exp_val*T)))
    return sum(revenues)/(exp_val*T)


def run_auction(a, bs):
    # weights = [[]] * number_of_bidders      # just copies the reference, which is bad!
    weights = [[] for _ in range(number_of_bidders)]
    for t in range(T):
        bids = []
        for b in bs:        # collect bids
            b.draw_valuation()
            bid = b.bid()
            bids.append(bid)
        allocations_and_prices = a.auction_round(bids)      # do the auction
        for i in range(number_of_bidders):                  # learn from auction
            allocation, price = allocations_and_prices[i]
            w = bs[i].learn(allocation, price)
            weights[i].append(w)
    return a.get_revenues(), weights


def weight_graph(colors, markers, arms, weights):
    handles = []
    labels = []
    num_bidders = range(len(weights))
    for bidder in num_bidders:
        bidder_weights = weights[bidder]
        for arm in arms:
            ys = [w[arm] for w in bidder_weights]
            h, = plt.plot(list(range(T)), ys, c=colors[(arm + int(bidder*len(colors)/number_of_bidders))% len(colors)], marker=markers[bidder % len(markers)],
                          linewidth=0, markersize=1, label="bidder {}, arm {}".format(bidder + 1, arm))
            if num_bidders == 1:
                labels.append("arm {}".format(arm))
            elif len(num_bidders) > 1:
                labels.append("bidder {} arm {}".format(bidder+1, arm))
            handles.append(h)
    if len(num_bidders) <= 2:        # legend gets too overcrowded when there's more than 1 or 2 bidder
        plt.legend(handles, labels)
    plt.xlabel("Iteration number")
    plt.ylabel("Weight")
    if len(num_bidders)  == 1:
        plt.title("Arm weight throughout iterations")
    else:
        plt.title("Arm weight throughout iterations for {} bidders".format(len(num_bidders)))
    plt.show()


def session_graph(a, arms):
    session_empty_rounds = a.session_empty_rounds  # sessioni[j] is the number of i rounds for arm j
    session_0_rounds = a.session_0_rounds

    for arm in arms:
        x_session_empty = list(range(int(session_empty_rounds[arm])))
        x_session_0 = list(
            range(int(session_empty_rounds[arm]), int(session_empty_rounds[arm] + session_0_rounds[arm])))
        x_session_1 = list(range(int(session_empty_rounds[arm] + session_0_rounds[arm]), T))

        y_session_empty = [arm] * len(x_session_empty)
        y_session_0 = [arm] * len(x_session_0)
        y_session_1 = [arm] * len(x_session_1)

        h1, = plt.plot(x_session_empty, y_session_empty, c='r')
        h2, = plt.plot(x_session_0, y_session_0, c='y')
        h3, = plt.plot(x_session_1, y_session_1, c='g')
        plt.legend([h1, h2, h3], ["Emptyset session", "0 session", "1 session"])

    plt.xlabel("Iteration number")
    plt.ylabel("Arm number")
    plt.title("Arm sessions through iterations")
    plt.show()


if __name__ == "__main__":
    np.random.seed(150989)

    number_of_bidders = 2

    # d_support = [0.25, 0.5, 1]
    # d_probs = [0.5, 0.25, 0.25]
    d_support = [0.5, 0.8]
    d_probs = [0.99, 0.01]

    T = 50000
    res = []
    epsilons = [0.001, 0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.5, 0.6, 0.7]
    # epsilons = [0.1]
    step_size = 0.0005
    show_graphs = False if len(epsilons) > 1 else True
    track_rounds = False
    bidder_chooser = np.argmin
    for epsilon in epsilons:
        res.append(experiment(number_of_bidders, d_support, d_probs, epsilon, T, step_size,
                              bidder_chooser, track_rounds, show_graphs=show_graphs))
    if len(epsilons) > 1:
        plt.xlabel("Epsilon")
        plt.ylabel("Achieved revenue divided by promised revenue")
        if number_of_bidders == 1:
            plt.title("Approximation quality as a function of epsilon")
        else:
            plt.title("Approximation quality as a function of epsilon for {} bidders".format(number_of_bidders))
        plt.scatter(epsilons, res)
        plt.show()

